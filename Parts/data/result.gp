set title "Result"
set autoscale
set grid
set style data linespoints
set key reverse Left outside
set terminal png size 800,300 

set output "circle_mapped.png"
plot 'circle_test.data' with points, 'circle_result.data'
set output "circle.png"
plot 'circle_result.data'

set output "square_mapped.png"
plot 'square_test.data' with points, 'square_result.data'
set output "square.png"
plot 'square_result.data'

set output "power_mapped.png"
plot 'power_x_test.data' with points, 'power_x_result.data'
set output "power.png"
plot 'power_x_result.data'

set output "sqrt_mapped.png"
plot 'sqrt_x_test.data' with points, 'sqrt_x_result.data'
set output "sqrt.png"
plot 'sqrt_x_result.data'

set output "log_mapped.png"
plot 'log_x_test.data' with points, 'log_x_result.data'
set output "log.png"
plot 'log_x_result.data'
