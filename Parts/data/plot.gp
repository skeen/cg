set title "Circle"
set autoscale
set grid
set style data linespoints
set key reverse Left outside
set terminal png size 800,300 
set output "out.png"
plot 'circle_test.data'
