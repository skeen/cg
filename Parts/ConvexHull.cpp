#include "ParseInput.hpp"

#include <cstdlib>
#include <iostream>
#include <chrono>
#include <algorithm>
#include <cassert>

template<typename T>
std::vector<T> append(std::vector<T> v1, const std::vector<T>& v2)
{
    for(auto elem : v2)
    {
        v1.push_back(elem);
    }
    return v1;
}

template<typename T>
std::vector<T> reverse(std::vector<T> v)
{
    std::vector<T> output;
    for(auto iter = v.end() - 1; iter >= v.begin(); --iter)
    {
        output.push_back(*iter);
    }
    return output;
}

bool is_above(const Point& pi, const Point& px, const Point& px_plus_1)
{
    // Calcualte a and b, for the line between pi and px_plus_1
    ft a = (pi.y - px_plus_1.y) / (pi.x - px_plus_1.x);
    ft b = pi.y - a * pi.x;
    // Find the lines y coordinate, and the points x coordinate
    ft check_y = a * px.x + b;
    // If the points y coordinate, is larger than the expected, then we're above
    // Otherwise, we're on the line or below
    return (check_y < px.y);
}

enum class SIDE
{
    LEFT,
    ON_LINE,
    RIGHT
};

auto upper_hull = [](bool above)
{
    if(above)
        return SIDE::LEFT;
    else
        return SIDE::RIGHT;
};

auto lower_hull = [](bool above)
{
    if(above)
        return SIDE::RIGHT;
    else
        return SIDE::LEFT;
};

#include "Part0.hpp"
#include "Part0Optim.hpp"
#include "Part1.hpp"
#include "PartB.hpp"
#include "PartC.hpp"

// Note: std::cerr is used for info messages.
// Note: std::cout is used for file (result) output.
int main(int /*argc*/, char* /*argv*/[])
{
    // Time the parsing process
    auto pre_parse = std::chrono::high_resolution_clock::now();
    // Do the actual parsing froms stdin
    std::vector<std::pair<int, Point>> points(parse_stdin());
    auto post_parse = std::chrono::high_resolution_clock::now();
    // Calculate the time difference (ie. duration)
    auto parse_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(post_parse - pre_parse).count();
    // And announce it to stdout
    std::cerr << "Parsing input took; " << parse_elapsed << " milliseconds" << std::endl;
    // Write out the number of points in the input
    std::cerr << "Input contains; " << points.size() << " points" << std::endl;

    auto pre_calculate = std::chrono::high_resolution_clock::now();
    // Calculate the convex hull
    // TODO: 3 algorithms
    //auto result0 = part0(points);
    //auto result0optim = part0optim(points);
    //auto result = part1(points);
    //auto resultb = partB(points);
    auto resultc = partC(points);
    auto post_calculate = std::chrono::high_resolution_clock::now();
    auto calculate_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(post_calculate - pre_calculate).count();
    std::cerr << "Calculation took; " << calculate_elapsed << " milliseconds" << std::endl;
    
    auto& result = resultc;
/*
    auto& cmp1 = result0;
    auto& cmp2 = resultb;
    assert(cmp1.size() == cmp2.size());
    assert(std::equal(cmp1.begin(), cmp1.end(), cmp2.begin()));
*/

    // Output everything to gnuplot render
    for(std::pair<int, Point> &pair : result)
    {
        Point &p = std::get<1>(pair);
        std::cout << p.x << " " << p.y << std::endl;
    }
    return EXIT_SUCCESS;
}
