// TODO: Right side sometimes misses a point
template<typename LAMBDA>
std::vector<std::pair<int, Point>> partB_combine(std::vector<std::pair<int, Point>> left_hull_in, std::vector<std::pair<int, Point>> right_hull, LAMBDA above_to_side)
{
    // The combined hull
    std::vector<std::pair<int, Point>> combined_hull;
    // Find the max 'y' element of each hull.
    auto left_hull = reverse(left_hull_in);
    auto left_iter = left_hull.begin();
    auto right_iter = right_hull.begin();
    while(true)
    {
        if(right_iter + 1 < right_hull.end())
        {
            auto right_next = right_iter + 1;
            SIDE which1 = above_to_side(is_above(std::get<1>(*left_iter), std::get<1>(*right_iter), std::get<1>(*right_next)));
            if(which1 == SIDE::RIGHT)
            {
                ++right_iter;
                continue;
            }
        }
        if(left_iter + 1 < left_hull.end())
        {
            auto left_next = left_iter + 1;
            SIDE which2 = above_to_side(is_above(std::get<1>(*left_next), std::get<1>(*left_iter), std::get<1>(*right_iter)));
            if(which2 == SIDE::RIGHT)
            {
                ++left_iter;
                continue;
            }
        }
        break;
    }

    for(auto iter = left_iter; iter < left_hull.end(); iter++)
    {
        combined_hull.push_back(*iter);
    }
    combined_hull = reverse(combined_hull);
 
    for(auto iter = right_iter; iter < right_hull.end(); iter++)
    {
        combined_hull.push_back(*iter);
    }   
   
    // Draw a line between them, and check if left max (y)+1 (on x) is on the left or right of this line.
    // - if it's on the left, go to it, and check for it's +1
    // - if it's on the right, drop it from the collection
    // Keep going till we hit left.x (max), then keep going with right.x (min) till the max (y) of right

    // Find the min 'y' element of each hull.
    // Draw a line between them, and check if left min (y)+1 (on x) is on the left or right of this line.
    // - if it's on the right, go to it, and check for it's +1
    // - if it's on the right, drop it from the collection
    // Keep going till we hit left.x (max), then keep going with right.x (min) till the min (y) of right.
    return combined_hull;
}

template<typename LAMBDA>
std::vector<std::pair<int, Point>> partB_worker(std::vector<std::pair<int, Point>> points, LAMBDA above_to_side)
{
    // We cannot split, as we need (as minimum) triangles to merge
    // Note; This is our base case
    if(points.size() <= 2)
    {
        return points;
    }
    else // We're able to split
    {
        // Divide
        std::size_t const half_size = points.size() / 2;
        std::vector<std::pair<int, Point>> split_lo(points.begin(), points.begin() + half_size);
        std::vector<std::pair<int, Point>> split_hi(points.begin() + half_size, points.end());
        // And ...
        auto hull_lo = partB_worker(split_lo, above_to_side);
        auto hull_hi = partB_worker(split_hi, above_to_side);
        // Conquer
        return partB_combine(hull_lo, hull_hi, above_to_side);
    }
}

std::vector<std::pair<int, Point>> partB(std::vector<std::pair<int, Point>> points)
{
    std::sort(points.begin(), points.end(), [](std::pair<int, Point> p1, std::pair<int, Point> p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });
    auto upper = partB_worker(points, upper_hull);
    auto lower = partB_worker(points, lower_hull);
    return append(upper, reverse(lower));
}
