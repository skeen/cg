#ifndef _PARSE_INPUT_HPP
#define _PARSE_INPUT_HPP

#include <cmath>
#include <vector>

// Define which floating type the program will be using
using floating_type = double;
using ft = floating_type;

// Representing a point in 2D space
class Point
{
    public:
    // Default constructor for default vector initialization
    Point()
        : x(NAN), y(NAN)
    {
    }

    // Emplace constructor
    Point(ft x, ft y)
        : x(x), y(y)
    {
    }

    bool operator==(const Point other) const
    {
        return other.x == x &&
               other.y == y;
    }

    // Data members
    ft x;
    ft y;
};

std::vector<std::pair<int, Point>> parse_stdin();

#endif //PARSE_INPUT
