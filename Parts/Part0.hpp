template<typename LAMBDA>
std::vector<std::pair<int, Point>> part0_worker(std::vector<std::pair<int, Point>> points, LAMBDA above_to_side)
{
    // Clean up time
    for(auto iter = points.begin(); iter < points.end() - 2;)
    {
        auto pi  = std::get<1>(*(iter+0));
        auto pi1 = std::get<1>(*(iter+1));
        auto pi2 = std::get<1>(*(iter+2));
        // If the middle point is to the right (i.e. inside)
        // Remove it and check again with iter+0
        if(above_to_side(is_above(pi, pi1, pi2)) == SIDE::RIGHT)
        {
            // Erase and get an iterator pointing to the new location
            // of the element that followed the last element erased by
            // the function call. 
            // ie. iter+2
            iter = points.erase(iter+1);
            // We actually want to continue checking from iter+0
            // So we jump two steps back
            iter = std::max(iter-2, points.begin());
        }
        // If it's to the left (i.e. outside)
        // Keep it, by letting this be iter+0 for now.
        else
        {
            ++iter;
        }
    }
    return points;
}

// TODO: const first argument, to ensure it's never changed
std::vector<std::pair<int, Point>> part0(std::vector<std::pair<int, Point>>& points)
{
    // Sort the input
    std::sort(points.begin(), points.end(), [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });

    auto upper = part0_worker(points, upper_hull);
    auto lower = part0_worker(points, lower_hull);
    return append(upper, reverse(lower));
}
