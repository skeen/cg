#include <cstdlib>
#include <random>
#include <cassert>
#include <iostream>
#include <cmath>

#include "../ParseInput.hpp"

// We need PI
constexpr double pi() { return std::atan(1)*4; }

// NOTE: This program is by no means memory/time efficient.
// Instead of streaming the output to the file,
// data is stored temporarily and then eventually flushed.
int main(int argc, char* argv[])
{
    // Randomness device, and generator
    std::random_device rd;
    std::mt19937 gen(rd());

    // Program need 3 arguments on the form;
    // ./generator TYPE NUMBER
    // where TYPE is; 'square', 'circle', alike.
    // and NUMBER is; the number of points to generate
    if(argc != 3)
    {
        std::cerr << "Usage: ./generator TYPE NUMBER" << std::endl;
        return EXIT_FAILURE;
    }

    // Read out the number of points to generate
    const int num_points = atoi(argv[2]);
    assert(num_points > 0);
    // Allocate memory for it in a vector
    std::vector<Point> points;
    points.reserve(num_points);

    // Generate points on a square
    if(std::string(argv[1]) == "square")
    {
        // Distribute randoms in the interval of 0 <= x < 100
        std::uniform_real_distribution<ft> dis(0.0, 100.0);

        // Random 'x' and 'y' value
        for(int i=0; i<num_points; i++)
        {
            points.emplace_back(dis(gen), dis(gen));
        }
    }
    // Generate points on a circle
    else if(std::string(argv[1]) == "circle")
    {
        // Distribute randoms in the interval of 0 <= x < 1
        std::uniform_real_distribution<ft> radius_dis(0.0, 1.0);

        // Distribute randoms in the interval of 0 <= x < 2*PI
        std::uniform_real_distribution<ft> theta_dis(0.0, 2*pi());

        // We calculate points within a circle by polar conversion
        // 'x' value given by; sqrt(r) * cos(theta)
        // 'y' value given by; sqrt(r) * sin(theta)
        // NOTE: Doing the usual r * cos(theta) is incorrect, due to origin area concentration.
        for(int i=0; i<num_points; i++)
        {
            const ft radius = radius_dis(gen);
            const ft theta = theta_dis(gen);
            const ft x = std::sqrt(radius) * std::cos(theta);
            const ft y = std::sqrt(radius) * std::sin(theta);
            points.emplace_back(x, y);
        }
    }
    // Generate points on the curve Y = X^2
    else if(std::string(argv[1]) == "power_x")
    {
        // Distribute randoms in the interval of -100 <= x < 100
        std::uniform_real_distribution<ft> dis(-100.0, 100.0);

        for(int i=0; i<num_points; i++)
        {
            const ft x = dis(gen);
            const ft y = std::pow(x,2);
            points.emplace_back(x, y);
        }
    }
    // Generate points on the curve Y = sqrt(X)
    else if(std::string(argv[1]) == "sqrt_x")
    {
        // Distribute randoms in the interval of 0 <= x < 100
        std::uniform_real_distribution<ft> dis(0.0, 100.0);

        for(int i=0; i<num_points; i++)
        {
            const ft x = dis(gen);
            const ft y = std::sqrt(x);
            points.emplace_back(x, y);
        }
    }
    // Generate points on the curve Y = log(X)
    else if(std::string(argv[1]) == "log_x")
    {
        // Distribute randoms in the interval of 0 <= x < 100
        std::uniform_real_distribution<ft> dis(0.0, 100.0);

        for(int i=0; i<num_points; i++)
        {
            const ft x = dis(gen);
            const ft y = std::log(x);
            points.emplace_back(x, y);
        }
    }
    else
    {
        std::cerr << "Error: No valid distribution given" << std::endl;
        return EXIT_FAILURE;
    }
 
    for(Point &p : points)
    {
        std::cout << p.x << " " << p.y << std::endl;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
