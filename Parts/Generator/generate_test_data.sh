#!/bin/bash

# The number of coordinates to generate
NUM_COORDS=$1
if [ -z "$NUM_COORDS" ]; then
    echo "Please provide the number of coords to generate to each data-set";
    exit -1;
fi

# Set the output folder
OUTPUT_DIR=../data

# Make the output folder
mkdir -p $OUTPUT_DIR

# Compile the generator program
g++ -O3 -Wall -Wextra --std=c++11 Generator.cpp -o generator

# Run the generate program and make samples
./generator square   $NUM_COORDS > $OUTPUT_DIR/square_test.data
./generator circle   $NUM_COORDS > $OUTPUT_DIR/circle_test.data
./generator power_x  $NUM_COORDS > $OUTPUT_DIR/power_x_test.data
./generator sqrt_x   $NUM_COORDS > $OUTPUT_DIR/sqrt_x_test.data
./generator log_x    $NUM_COORDS > $OUTPUT_DIR/log_x_test.data
