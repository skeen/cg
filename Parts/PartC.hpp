
template<typename LAMBDA>
std::vector<std::pair<int, Point>> partC_worker(std::vector<std::pair<int, Point>> points, LAMBDA above_to_side)
{
    // 0. Base case, to end recursion
    /* // TODO: Enable the below (we'll need the simplex algorithm by then)
    if(points.size() < 2)
    {
        return points;
    }
    */
    if(points.size() < 5)
    {
        std::sort(points.begin(), points.end(), [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });

        return part0_worker(points, above_to_side);
    }

    // 1. Find the point with median x coordinate pm = (xm, y)
    auto median = [](std::vector<std::pair<int, Point>> &v)
    {
        const size_t n = v.size() / 2;
        std::nth_element(v.begin(), v.begin()+n, v.end(), 
                [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
                {
                    return std::get<1>(p1).x < std::get<1>(p2).x;
                });
        return v[n];
    };
    const auto median_pair = median(points);
    const auto median_point = std::get<1>(median_pair);
    const ft x_median = median_point.x;

    // 1.1 Partition the input into two sets P` and Pr where;
    // * P` contains all the points with x-coordinate smaller than xm
    // * Pr contains the rest of the points
    std::vector<std::pair<int, Point>> split_lo;
    std::vector<std::pair<int, Point>> split_hi;
    for(auto& pair : points)
    {
        const auto& point = std::get<1>(pair);
        const ft x = point.x;
        if(x < x_median)
        {
            split_lo.push_back(pair);
        }
        else
        {
            split_hi.push_back(pair);
        }
    }

    // MbC2_CH; Find the point p` with the smallest x-coordinate
    // (if there are more than one, take the one with the largest y-coordinate) 
    // and the point pr with the largest x-coordinate
    // (if there are more than one, take the one with the smallest y-coordinate).
    // Note that these can be done at the same time as step 1.
    // Prune all the points that lie under the line segment p`pr.
    // TODO: Is not function, may be due to missing simplex
    //#define MbC2_CH
    #ifdef MbC2_CH
    auto smallest_pair = *std::min_element(split_lo.begin(), split_lo.end(),
             [](const std::pair<int, Point>& pa1, const std::pair<int, Point>& pa2)
             {
                const auto& p1 = std::get<1>(pa1);
                const auto& p2 = std::get<1>(pa2);
                if(p1.x == p2.x)
                {
                    return p1.y > p2.y;
                }
                else
                {
                    return p1.x < p2.y;
                }
             });
    auto smallest_point = std::get<1>(smallest_pair);

    auto largest_pair = *std::max_element(split_hi.begin(), split_hi.end(),
             [](const std::pair<int, Point>& pa1, const std::pair<int, Point>& pa2)
             {
                const auto& p1 = std::get<1>(pa1);
                const auto& p2 = std::get<1>(pa2);
                if(p1.x == p2.x)
                {
                    return p1.y < p2.y;
                }
                else
                {
                    return p1.x < p2.y;
                }
             });
    auto largest_point = std::get<1>(largest_pair);
    
    auto prune = [&](std::vector<std::pair<int, Point>>& vec)
    {
        for(auto iter = vec.begin(); iter != vec.end();)
        {
            const auto& pair = *iter;
            const auto& point = std::get<1>(pair);
            if(above_to_side(is_above(smallest_point, point, largest_point)) == SIDE::RIGHT)
            {
                iter = points.erase(iter);
            }
            else
            {
                iter++;
            }
        }
    };
    prune(split_lo);
    prune(split_hi);
    #endif 

    // 2. Find the “bridge” over the vertical line X = xm
    // (i.e., the upper hull edge that intersects line X = xm).
    // You need to implement linear programming for this step.
    // Let (xi, yi) and (xj, yj) be the left and right end points of the bridge.
    //
    // TODO: minimize vs maximize for upper/lower hull
    // minimize alfa * a + beta
    // subject to:
    //   alfa * pi.x + beta >= pi.y
    //  \forall pi \in L \cup R.
    //
    //  alfa = (xi - xj) / (yi - yj)
    //  beta = ...
    
    // TODO: Simplex rather than part0
    auto copy_lo = split_lo;
    auto copy_hi = split_hi;
    std::sort(copy_lo.begin(), copy_lo.end(), [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });

    std::sort(copy_hi.begin(), copy_hi.end(), [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });

    auto hull_left = part0_worker(copy_lo, above_to_side);
    auto hull_right = part0_worker(copy_hi, above_to_side);

    auto left_iter = hull_left.rbegin();
    auto right_iter = hull_right.begin();
    while(true)
    {
        if(right_iter + 1 < hull_right.end())
        {
            auto right_next = right_iter + 1;
            SIDE which1 = above_to_side(is_above(std::get<1>(*left_iter), std::get<1>(*right_iter), std::get<1>(*right_next)));
            if(which1 == SIDE::RIGHT)
            {
                ++right_iter;
                continue;
            }
        }
        if(left_iter + 1 < hull_left.rend())
        {
            auto left_next = left_iter + 1;
            SIDE which2 = above_to_side(is_above(std::get<1>(*left_next), std::get<1>(*left_iter), std::get<1>(*right_iter)));
            if(which2 == SIDE::RIGHT)
            {
                ++left_iter;
                continue;
            }
        }
        break;
    }
    
    const ft xi_x = std::get<1>(*left_iter).x;
    const ft xj_x = std::get<1>(*right_iter).x;
    assert(xi_x < xj_x);
    assert(xi_x <= x_median && x_median <= xj_x);
/*
    std::cerr << "xi_x " << xi_x << std::endl;
    std::cerr << "xj_x " << xj_x << std::endl;
    std::cerr << "median" << x_median << std::endl;
*/
    // 3. Prune the points that lie under the line segment (xi, yi), (xj, yj)
    // (these will be the points whose x-coordinates lie between xi and xj).
    for(auto iter = split_lo.begin(); iter < split_lo.end();)
    {
        const auto& pair = *iter;
        const auto& point = std::get<1>(pair);
        const ft x_coord = point.x;
        if(xi_x < x_coord)
        {
            iter = split_lo.erase(iter);
        }
        else
        {
            iter++;
        }
    }
    for(auto iter = split_hi.begin(); iter < split_hi.end();)
    {
        const auto& pair = *iter;
        const auto& point = std::get<1>(pair);
        const ft x_coord = point.x;
        if(x_coord < xj_x)
        {
            iter = split_hi.erase(iter);
        }
        else
        {
            iter++;
        }
    }
    // 4. Recursively compute the upper hull of P` and Pr.
    auto lo_hull = partC_worker(split_lo, above_to_side);
    auto hi_hull = partC_worker(split_hi, above_to_side);
    return append(lo_hull, hi_hull);
}

std::vector<std::pair<int, Point>> partC(std::vector<std::pair<int, Point>> points)
{
    auto upper = partC_worker(points, upper_hull);
    auto lower = partC_worker(points, lower_hull);
    return append(upper, reverse(lower));
}
