#!/bin/bash

# Build general files
g++ -Wall -Wextra --std=c++11 -g ParseInput.cpp -c -o ParseInput.o

# Build executable
g++ -Wall -Wextra --std=c++11 -g ConvexHull.cpp -c -o ConvexHull.o
g++ -Wall -Wextra --std=c++11 -g ConvexHull.o ParseInput.o -o ConvexHull
