#include <list>

template<typename T>
std::vector<T> to_vector(std::list<T> list)
{
    return std::vector<T>(list.begin(), list.end());
}

template<typename T>
std::list<T> to_list(std::vector<T> vec)
{
    return std::list<T>(vec.begin(), vec.end());
}
    
// TODO: Badly broken
template<typename LAMBDA>
std::list<std::pair<int, Point>> part0optim_worker(std::list<std::pair<int, Point>> list, LAMBDA above_to_side)
{
    // The rightmost point
    auto rm = std::get<1>(*(list.rbegin()));
    // Clean up time
    // TODO: iter may be larger than --list.end()-- at start
    for(auto iter = list.begin(); iter != --list.end()--;)
    {
        auto pi  = std::get<1>(*(iter));
        auto pi1 = std::get<1>(*(iter++));
        // If the middle point is to the right (i.e. inside)
        // Remove it and check again with iter+0
        if(above_to_side(is_above(pi, pi1, rm)) == SIDE::RIGHT)
        {
            // Erase and get an iterator pointing to the new location
            // of the element that followed the last element erased by
            // the function call. 
            // ie. iter+2
            iter = list.erase(iter);
            iter--;
        }
        // If it's to the left (i.e. outside)
        // Keep it, by letting this be iter+0 for now.
    }
    return list;
}

// TODO: const first argument, to ensure it's never changed
std::vector<std::pair<int, Point>> part0optim(std::vector<std::pair<int, Point>>& points)
{
    // Sort the input
    std::sort(points.begin(), points.end(), [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });

    auto points_list = to_list(points);
    auto upper = part0optim_worker(points_list, upper_hull);
    auto lower = part0optim_worker(points_list, lower_hull);
    return append(to_vector(upper), reverse(to_vector(lower)));
}
