#include "ParseInput.hpp"

#include <iostream>

std::vector<std::pair<int, Point>> parse_stdin()
{
    // Container, which we'll fill
    std::vector<std::pair<int, Point>> points;
    // Start parsing
    for(int i=1;; i++)
    {
        // These are what we'll parse into
        double x;
        double y;
        // Do the actual parsing
        std::cin >> x;
        std::cin >> y;
        // If we did not succeed, stop parsing
        if(std::cin.good() == false)
        {
            break;
        }
        // If we did succeed, add the point, and try parsing another
        points.emplace_back(i, Point(x,y));
    }
    return points;
}
