/*
template<typename LAMBDA>
std::vector<std::pair<int, Point>> part1_worker(std::vector<std::pair<int, Point>>& points, LAMBDA above_to_side)
{
    std::vector<std::pair<int, Point>> output;
    // Pick out the first point
    // pi is the last point added to the hull
    auto pi = points.at(0);
    output.push_back(pi);
    // Start running
    for(unsigned int x = 1; x < points.size(); x++)
    {
        // Get the point we're at
        auto px = points.at(x);
        // Ensure that we've got enough points to check
        if(!(x+1 < points.size()))
        {
            // x must be the last point, and hence in the hull
            output.push_back(px);
            break;
        }
        // Get the point we need
        auto px_plus_1 = points.at(x+1);
        // Check whether px is above the line between pi and px+1
        SIDE which = above_to_side(is_above(std::get<1>(pi), std::get<1>(px), std::get<1>(px_plus_1)));
        // If it is indeed above, then px needs to be in the convex hull
        if(which == SIDE::LEFT)
        {
            output.push_back(pi);
            pi = px;
        }
        // If it is not above, then it must be below or on the line
        // hence it is not in the convex hull, and we continue our scan
    }
    return output;
}

// TODO: const first argument, to ensure it's never changed
std::vector<std::pair<int, Point>> part1(std::vector<std::pair<int, Point>>& points)
{
    // Sort the input
    std::sort(points.begin(), points.end(), [](const std::pair<int, Point>& p1, const std::pair<int, Point>& p2)
            {
                return std::get<1>(p1).x < std::get<1>(p2).x;
            });

    auto upper = part1_worker(points, upper_hull);
    auto lower = part1_worker(points, lower_hull);

    for(int x = lower.size() - 1; x >= 0; --x)
    {
        auto elem = lower.at(x);
        upper.push_back(elem);
    }

    return upper;
}
*/


