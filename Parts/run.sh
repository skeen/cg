#!/bin/bash

NUM_COORDS=$1
if [ -z "$NUM_COORDS" ]; then
    NUM_COORDS=100
fi

EXECUTEABLE=./ConvexHull

cd Generator
./generate_test_data.sh $NUM_COORDS
cd ..
echo "Square test"
cat data/square_test.data | $EXECUTEABLE > data/square_result.data
echo "Circle test"
cat data/circle_test.data | $EXECUTEABLE > data/circle_result.data
echo "Power test"
cat data/power_x_test.data | $EXECUTEABLE > data/power_x_result.data
echo "Sqrt test"
cat data/sqrt_x_test.data | $EXECUTEABLE > data/sqrt_x_result.data
echo "Log test"
cat data/log_x_test.data | $EXECUTEABLE > data/log_x_result.data

cd data
gnuplot result.gp
